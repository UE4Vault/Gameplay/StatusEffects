// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/DamageType.h"
#include "GameFramework/Actor.h"
#include "StatusEffects.h"
#include "BaseStatusEffect.generated.h"

/**
 * 
 */
UCLASS()
class ALTSTATUSEFFECTS_API UBaseStatusEffect : public UDamageType
{
	GENERATED_BODY()
	
public:

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
		void CustomEffect() const;

	//The name of the status effect
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FName Name = "BaseStatusEffect";
	
	//How many uses this effect has
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int Strength = 10;

	//How much damage is done each time one strength of the effect is used
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int Damage = 10;

	//The controller that caused this
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		AController* EventInstigator = nullptr;

	//The actor that caused this
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		AActor* DamageCauser = nullptr;
};
