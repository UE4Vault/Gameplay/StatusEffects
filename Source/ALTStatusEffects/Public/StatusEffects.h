// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/World.h"
#include "Kismet/KismetMathLibrary.h"
#include "StatusEffects.generated.h"

class UBaseStatusEffect;

DECLARE_LOG_CATEGORY_EXTERN(LogStatusEffects, Log, All);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class ALTSTATUSEFFECTS_API UStatusEffects : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UStatusEffects();


	// Functions


	//Damages the owner
	UFUNCTION(BlueprintCallable)
		void Damage();

	//Removes all currently active effects and clears the timers
	UFUNCTION(BlueprintCallable)
		void RemoveAll();

	//Handles creating the effect
	UFUNCTION(BlueprintCallable)
		UBaseStatusEffect* Handle(float Damage, UDamageType* DamageType, AController* EventInstigator, AActor* DamageCauser, bool Manual = false);

	//Updates the timer
	UFUNCTION(BlueprintCallable)
		void UpdateTimer();

	//Updates the currently active effects
	UFUNCTION(BlueprintCallable)
		void Update(UBaseStatusEffect* NewEffect, bool Remove = false);

	UFUNCTION(BlueprintCallable, meta = (DeterminesOutputType = "StatusEffectType"))
		UBaseStatusEffect* Create(TSubclassOf<UBaseStatusEffect> StatusEffectType, int Strength, int Damage, AController* EventInstigator, AActor* DamageCauser);


	// Properties


	//The effects that are currently active
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		TArray<UBaseStatusEffect*> ActiveEffects;

	//The effects that can't be applied to this character
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<UBaseStatusEffect*> Immunities;



protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

		
	
};
