// Fill out your copyright notice in the Description page of Project Settings.

#include "StatusEffects.h"
#include "ALTStatusEffects/Public/BaseStatusEffect.h"

DEFINE_LOG_CATEGORY(LogStatusEffects);

// Sets default values for this component's properties
UStatusEffects::UStatusEffects()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


void UStatusEffects::Damage()
{
	UBaseStatusEffect* LActiveEffect;

	for (int i = 0; i < ActiveEffects.Num(); i++)
	{
		//Check if the active effects index is valid
		LActiveEffect = ActiveEffects.IsValidIndex(i) ? ActiveEffects[i] : Cast<UBaseStatusEffect>(UBaseStatusEffect::StaticClass());
		//Do nothing if the active effects index is invalid
		if (LActiveEffect == Cast<UBaseStatusEffect>(UBaseStatusEffect::StaticClass()))
			break;

		//Do nothing if ActiveEffects contains an immunity
		if (!Immunities.Contains(LActiveEffect))
		{
			//Apply one effect use to owner
			UGameplayStatics::ApplyDamage(GetOwner(), LActiveEffect->Damage, LActiveEffect->EventInstigator, LActiveEffect->DamageCauser, UDamageType::StaticClass());

			//Lower strength/uses of effect by one
			LActiveEffect->Strength = LActiveEffect->Strength - 1;

			UE_LOG(LogStatusEffects, Log, TEXT("Updated strength %d"), LActiveEffect->Strength);

			//Activate our status effect (if it has a custom effect)
			LActiveEffect->CustomEffect();

			if (LActiveEffect->Strength <= 0)
				{
					ActiveEffects.RemoveAt(i);
					UKismetSystemLibrary::K2_ClearTimer(this, "Damage");
				}
			UpdateTimer();
		}
	}
}

void UStatusEffects::RemoveAll()
{
	ActiveEffects.Empty();
	UpdateTimer();
}

UBaseStatusEffect* UStatusEffects::Handle(float Damage, UDamageType* DamageType, AController* EventInstigator, AActor* DamageCauser, bool Manual)
{
	UBaseStatusEffect* StatusEffect = Cast<UBaseStatusEffect>(DamageType);

	//Do nothing if they aren't valid
	if (!(StatusEffect->IsValidLowLevel() && EventInstigator->IsValidLowLevel() && DamageCauser->IsValidLowLevel()))
		return nullptr;

	//Create a new UBaseStatusEffect object and set it to have these values
	StatusEffect->EventInstigator = EventInstigator;
	StatusEffect->DamageCauser = DamageCauser;
	StatusEffect->Name = Cast<UBaseStatusEffect>(StatusEffect->GetClass()->GetDefaultObject())->Name;
	StatusEffect->Strength = UKismetMathLibrary::Round(Damage);

	//Toggle for if we want to manually update
	if (!Manual)
		Update(StatusEffect);

	return StatusEffect;
}

void UStatusEffects::UpdateTimer()
{
	if (ActiveEffects.Num() != 0)
		UKismetSystemLibrary::K2_SetTimer(this, "Damage", 1, true);
	else
		UKismetSystemLibrary::K2_ClearTimer(this, "Damage");
}

void UStatusEffects::Update(UBaseStatusEffect* NewEffect, bool Remove)
{
	//Add or Remove?
	if (!Remove)
	{
		bool Contained = false;

		//Resets all current Effects of this type to the new strength
		for (int i = 0; i < ActiveEffects.Num(); i++)
		{
			if (ActiveEffects[i]->Name.IsEqual(NewEffect->Name))
			{
				Contained = true;
			}
			if (ActiveEffects[i]->Name.IsEqual(NewEffect->Name) && ActiveEffects[i]->Strength < NewEffect->Strength)
			{
				ActiveEffects[i] = NewEffect;
			}
		}
		//Checks if ActiveEffects contains the NewEffect being added already
		if (Contained)
		{
			UE_LOG(LogStatusEffects, Warning, TEXT("ActiveEffects already contains NewEffect"));
		}
		//Prevent adding new effects with less than 1 strength
		else if (NewEffect->Strength >= 1)
		{
			ActiveEffects.AddUnique(NewEffect);
		}
		//If strength is 0, add default effect (At strength 1)
		else if (NewEffect->Strength == 0)
		{
			NewEffect->Strength = 1;
			ActiveEffects.AddUnique(NewEffect);
		}
	}
	else
	{
		//If strength = 0, remove first element
		if (NewEffect->Strength == 0)
		{
			for (int i = 0; i < ActiveEffects.Num(); i++)
			{
				if (ActiveEffects[i]->Name.IsEqual(NewEffect->Name))
				{
					ActiveEffects.RemoveAt(i);
					break;
				}
			}
		}
		else
		{
			ActiveEffects.Remove(NewEffect);
		}
	}

	UpdateTimer();
}

UBaseStatusEffect* UStatusEffects::Create(TSubclassOf<UBaseStatusEffect> StatusEffectType, int Strength, int Damage, AController * EventInstigator, AActor * DamageCauser)
{
	//Return created StatusEffect object class if true. Return default UBaseStatusEffect class if false.
	UBaseStatusEffect* StatusEffect = StatusEffectType ? StatusEffectType->GetDefaultObject<UBaseStatusEffect>() : Cast<UBaseStatusEffect>(UBaseStatusEffect::StaticClass());

	if (Strength > 0)
		StatusEffect->Strength = Strength;
	if (Damage > 0)
		StatusEffect->Damage = Damage;
	StatusEffect->EventInstigator = EventInstigator ? EventInstigator : Cast<AController>(AController::StaticClass());
	StatusEffect->DamageCauser = DamageCauser ? DamageCauser : Cast<AController>(AController::StaticClass());

	return StatusEffect;
}

// Called when the game starts
void UStatusEffects::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UStatusEffects::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

