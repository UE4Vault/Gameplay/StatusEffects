## Setup
1. Add the StatusEffects component to your actor
2. Use UBaseStatusEffect as the child class to create new status effect types
3. Use ApplyDamage to activate the status effect and then cast to the BaseStatusEffect
4. After, you can call the Handle function to set the status effect up

Alternitavely you can use the Create function to manually add status effects

If you want to make your new status effect type do something, override the custom effect function

## Current Version
`Unreal Engine 4.19`